package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class WebproductsApplicationTests {

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testProduct() {
		Product p = new Product();
		
		p.setId(1);
		
		assertThat(p.getId()).isEqualTo(1);
	}

}
