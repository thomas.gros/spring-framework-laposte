package com.example.demo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

// possible de changer le path segment si Spring Data Rest est utilisé
// voir: https://stackoverflow.com/questions/24577400/how-can-i-map-a-spring-boot-repositoryrestresource-to-a-specific-url

public interface ProductRepository extends JpaRepository<Product, Integer> {

	public List<Product> findByTitle(String title);
	public List<Product> findByTitleAndPrice(String title, String price);
}
