package com.example.demo;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
public class ProductRestController {

	@Autowired
	private ProductRepository productRepository;

	@RequestMapping(method=RequestMethod.GET, path="/hello")
	public String hello() {
		return "hello";
	}
	
	@GetMapping(path="/products")
	public List<Product> getProducts() {
		List<Product> products = productRepository.findAll();
		return products;
	}
	
	// TODO externaliser ProductNotFoundException
	@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Product not found")
	public class ProductNotFoundException extends RuntimeException {
	}
	
	@GetMapping(path="/products/{id}")
	public Product getProductById(@PathVariable("id") Integer id) {
		
		return productRepository.findById(id)
							    .orElseThrow(() -> new ProductNotFoundException());
	}
	
	
}
