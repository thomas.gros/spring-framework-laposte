package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/web")
public class ProductController {

	@Autowired
	private ProductRepository productRepository;

//  autre manière d'injecter le productRepository
//	public ProductController(ProductRepository productRepository) {
//		this.productRepository = productRepository;
//	}

	@RequestMapping(method=RequestMethod.GET, path="/hello")
	public String hello(Model model) {
		model.addAttribute("name", "Thomas");
		return "hello";
	}
	
	@GetMapping(path="/products")
	public String getProducts(Model model) {
		List<Product> products = productRepository.findAll();
		model.addAttribute("products", products);
		return "product-list";
	}
	
	
	
}
