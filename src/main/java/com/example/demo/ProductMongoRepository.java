package com.example.demo;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductMongoRepository 
	extends MongoRepository<ProductMongo, String>{

}
